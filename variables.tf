variable "terraversion" {
  description = "Version of the Terraform Script"
  default = "0.0.1"
}

variable "ownerteam" {
  description = "Which team is at fault here."
  default = "DevOps Team"
}

variable "public_key_path" {
  description = "SSH Key Path"
  default = "~/.ssh/DevKey.pem"
}

variable "key_name" {
  description = "Desired name of AWS key pair"
  default = "Devops Demonstration KP"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-west-1"
}

variable "aws_amis" {
  default = {
    us-west-1 = "ami-e0ba5c83"
  }
}

variable "awskey" {
  description = "AWS CLI Key ID - DEV account"
  default = "AKIAJGD4VUB3GX2OJICQ"
}
variable "awskeysecret" {
  description = "AWS CLI Secret Key - DEV account"
  default = "pmnsAGudyQF080CBFk89c6G4OZQfa0mUOiBzB1DG"
}
