# Terraform Test Code

This is a quick rebuild of a piece of Terraform/AWS work I did at Adaptive Insights.  Rewrote it from scratch so that I would not break any "I copied this from them" 

Not 100% happy with this, there's an issue with Route53 that I cannot solve in terraform and I don't recall the solution but it prevents the launch of the cluster due to how kops works.

I did not include any other machines or infrastructure other than the bare minimum to get a baseline "nothing in the region" setup.  So this will set up VPC and Router and Security Groups.