# Specify the provider and access details
provider "aws" {
  region = "${var.aws_region}"
}

# Create a VPC to launch our instances into
resource "aws_vpc" "default" {
  cidr_block = "10.38.0.0/16"
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

# Create a subnet to launch our instances into
resource "aws_subnet" "default" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.38.1.0/24"
  map_public_ip_on_launch = true
}

# Create S3 Bucket for Kops/Kubectl
resource "aws_s3_bucket" "b" {
  bucket = "clusters.kubes.demonstration.int"
  acl    = "private"

  tags {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

# Create a Route53 Zone for Kops/Kubectl
resource "aws_route53_zone" "main" {
  name = "demonstration.int"
}
resource "aws_route53_zone" "dev" {
  name = "uswest1a.kubes.demonstration.int"
}

resource "aws_route53_record" "dev-ns" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "uswest1a.kubes.demonstration.int"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.dev.name_servers.0}",
    "${aws_route53_zone.dev.name_servers.1}",
    "${aws_route53_zone.dev.name_servers.2}",
    "${aws_route53_zone.dev.name_servers.3}",
  ]
}

# Our default security group to access the instances.
resource "aws_security_group" "default" {
  name        = "Default Dev Group"
  description = "Main inbound SG."
  vpc_id      = "${aws_vpc.default.id}"

  # All access from home IP address 
  ingress {
    from_port   = 0 
    to_port     = 0 
    protocol    = "-1"  
    cidr_blocks = ["71.95.143.210/32"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

############################################
resource "aws_instance" "kops" 
{
# The kops management instance.
# 

  instance_type = "t2.medium"
  count = 1
  ami = "${lookup(var.aws_amis, var.aws_region)}"
#  disable_api_termination = "true"

  key_name = "DevTestKey"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]
  subnet_id = "${aws_subnet.default.id}"

  tags {
    Name = "KubeController"
    Type = "Utility"
    BuildVersion  = "${var.terraversion}"
    Team = "${var.ownerteam}"
  }

  # Configuration scripts, in order of execution
   provisioner "remote-exec" {
     scripts = [ 
	"./scripts/setup_devop_user.sh",
	"./scripts/setup_default_tuning.sh",
	"./scripts/setup_aws.sh",
	"./scripts/setup_kops.sh"
     ]
     connection {
      type = "ssh"
      user = "ec2-user"
      agent = true
     }
   }


}
