#!/usr/local/bin/python

# python script to manage the starting and stopping of instance types based upon name/name wildcard.


import boto3
import sys


if (len(sys.argv)<3) :
	print("Invalid number of arguments:\n USAGE: powerswitch.py <name pattern> <start|stop>")
	sys.exit()

ec2 = boto3.resource('ec2')

if ((sys.argv[2]!="stop") and (sys.argv[2]!="start")):
	print("Invalid start/stop argument:\n USAGE: powerswitch.py <name pattern> <start|stop>")
	sys.exit()

if (sys.argv[2]=="stop"):
	FilterState='running'
if (sys.argv[2]=="start"):
	FilterState='stopped'

inst_filter =  [{'Name':'tag:Name', 'Values':[sys.argv[1]]},{'Name':'instance-state-name', 'Values':[FilterState]}]
insts = list(ec2.instances.filter(Filters=inst_filter))
for inst in insts:
	if (sys.argv[2]=="stop"):
	  print("Stopping: " + inst.id)
	  inst.stop()
	if (sys.argv[2]=="start"):
	  print("Starting: " + inst.id)
	  inst.start()

