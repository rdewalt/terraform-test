# AWS Tools

* powerswitch.py  - Turns instances off/on based upon name pattern.

Requires:  awscli (and configured), python + boto3

Usage: powerswitch.py <NamePattern> [start|stop]

Example: $ python powerswitch.py MDS-L-* stop

Will stop all instances with the name MDS-L-01 through MDS-L-05 (and so on.)

* ebs_wipe.py - Deletes all unattached EBS volumes.

Does as the label states. no Example.  Dangerous Tool.