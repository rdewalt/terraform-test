#!/bin/bash
sudo bash <<EOF
echo "Beginning default basic tuning"
mount -o remount,noatime,discard,nobarrier /
echo never > /sys/kernel/mm/transparent_hugepage/enabled
echo 2 > /sys/block/*/queue/rq_affinity
echo noop > /sys/block/*/queue/scheduler
echo 256 > /sys/block/*/queue/nr_requests
echo 256 > /sys/block/*/queue/read_ahead_kb
echo tsc > /sys/devices/system/clocksource/clocksource0/current_clocksource
sysctl vm.swappiness=0
sysctl vm.dirty_ratio=80         
sysctl vm.dirty_background_ratio=5
sysctl vm.dirty_expire_centisecs=12000
sysctl net.core.somaxconn=32768
sysctl net.core.netdev_max_backlog=16384
sysctl net.core.rmem_max=16777216
sysctl net.core.wmem_max=16777216
sysctl net.ipv4.tcp_wmem="4096 12582912 16777216"
sysctl net.ipv4.tcp_rmem="4096 12582912 16777216"
sysctl net.ipv4.tcp_max_syn_backlog=8096
sysctl net.ipv4.tcp_slow_start_after_idle=0
sysctl net.ipv4.tcp_tw_reuse=1
sysctl net.ipv4.ip_local_port_range="10240 65535"
sysctl net.ipv4.tcp_abort_on_overflow=1 
echo "Default tuning Complete"
exit 0
EOF
