#!/bin/bash
sudo bash <<EOF
echo "Beginning 'KOPS' setup"
wget https://github.com/kubernetes/kops/releases/download/1.8.1/kops-linux-amd64
chmod +x kops-linux-amd64
mv kops-linux-amd64 /usr/bin/kops
export KOPS_STATE_STORE=s3://clusters.kubes.demonstration.int
echo export KOPS_STATE_STORE=s3://clusters.kubes.demonstration.int >> ~/.bash_profile
kops create cluster --zones=us-west-1a uswest1a.kubes.demonstration.int --yes
kops update cluster --name=uswest1a.kubes.demonstration.int --yes
kops rolling-update cluster --name=uswest1a.kubes.demonstration.int --yes --cloudonly
echo "Setup Complete"
exit 0
EOF
