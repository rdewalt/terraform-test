#!/bin/bash
sudo bash <<EOF
echo "Beginning 'devop' user setup"
# password is simply 'password' for this example.
useradd -m -p $1$xLN8Pa0q$qwgPVyfRhCCv7r4w64fu7/ -s /bin/bash devop
echo "devop ALL=(ALL) ALL" >> /etc/sudoers
echo "Match User devop"  >> /etc/ssh/sshd_config
echo "  PasswordAuthentication yes"  >> /etc/ssh/sshd_config
mkdir -p /home/devop/.ssh/
ssh-keygen -f /home/devop/.ssh/id_rsa -t rsa -N ''
chmod 600 /home/devop/.ssh/id*
chown devop:devop /home/devop/.ssh/*
mkdir -p /home/ec2-user/.ssh/
ssh-keygen -f /home/ec2-user/.ssh/id_rsa -t rsa -N ''
chmod 600 /home/ec2-user/.ssh/id*
chown ec2-user:ec2-user /home/ec2-user/.ssh/*
echo "Setup Complete"
exit 0
EOF
